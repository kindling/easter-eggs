<?php

if (!function_exists('add_action')) {
    return;
}

/**
 * Kindling Easter Eggs - Actions.
 *
 * @package KINDLING_EASTER_EGGS
 */

add_action('wp_head', function () {
    include_once __DIR__ . '/views/awesome-font.php';
});
