<?php
if (!isset($_REQUEST['awesome-font'])) {
    return;
}

$awesomeFont = $_REQUEST['awesome-font'];

$fonts = collect([
    'papyrus' => 'Papyrus, fantasy',
    'comic-sans' => "'Comic Sans MS', cursive, sans-serif",
]);
$font = $fonts->get($awesomeFont, $fonts->random());
?>
<style>
    body *,
    body {font-family: <?php echo $font; ?> !important;}
</style>
